package com.moghadam.apartmentProject.BankAccountTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.moghadam.apartmentProject.dao.BankAccountRepository;
import com.moghadam.apartmentProject.entity.BankAccount;
import com.moghadam.apartmentProject.entity.Location;
import com.moghadam.apartmentProject.service.BankAccountServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class BankAccountServiceUnitTest {
	
	@Mock
	private BankAccountRepository bankAccountRepository;
	
	@InjectMocks
	private BankAccountServiceImpl bankAccountService; 
	
	@Test
	public void saveBankAccount() {
		
		BankAccount bankAccount=new BankAccount(3,"melat","98465465","54848465877");
		
		when(bankAccountRepository.save(bankAccount)).thenReturn(bankAccount);
		
		Assert.assertNotNull(bankAccount);
		
		Assert.assertEquals(bankAccountService.save(bankAccount),bankAccount.getBankName());
		Assert.assertEquals("melat",bankAccount.getBankName());
		Assert.assertEquals("54848465877",bankAccount.getCardNumber());
		
	}
	
	
	@Test
	public void getAllBankAccount() {
		BankAccount bankAccount1=new BankAccount(2,"melat","98465465","54848465877");
		BankAccount bankAccount2=new BankAccount(3,"saderat","4965654","879846516");
		BankAccount bankAccount3=new BankAccount(4,"mali","879841","849465584954");
		
		
		List list=new ArrayList<>();
		list.add(bankAccount1);
		list.add(bankAccount2);
		list.add(bankAccount3);
		
		when(bankAccountRepository.findAll()).thenReturn(list);
		
		Assert.assertEquals(bankAccountService.findAll(),list);
		
		Assert.assertEquals(3, list.size());
		
	}
	
	
	@Test
	public void findBankAccountById() {
		
		BankAccount bankAccount=new BankAccount(3,"saderat","4965654","879846516");
		when(bankAccountRepository.findBankAccount(3)).thenReturn(bankAccount);
		
		
		BankAccount bankAccount2 = bankAccountService.findBankAccountById(3);
		Assert.assertEquals(bankAccountService.findBankAccountById(3),bankAccount2);
		
		Assert.assertEquals("saderat", bankAccount2.getBankName());
		Assert.assertEquals("4965654", bankAccount2.getAccountNumber());	
		
	}

}
