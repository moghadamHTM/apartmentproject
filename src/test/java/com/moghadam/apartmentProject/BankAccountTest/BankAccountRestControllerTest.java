package com.moghadam.apartmentProject.BankAccountTest;

import java.util.List;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.moghadam.apartmentProject.dao.BankAccountRepository;
import com.moghadam.apartmentProject.entity.BankAccount;
import com.moghadam.apartmentProject.entity.Person;
import com.moghadam.apartmentProject.service.BankAccountService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class BankAccountRestControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private BankAccountService bankAccountService;
	
	
	HttpHeaders headers = new HttpHeaders();
	
	@Test
	public void saveBankAccount() throws JSONException {
		
		BankAccount bankAccount=new BankAccount("melat","98465465","54848465877");
		HttpEntity<BankAccount> entity = new HttpEntity<BankAccount>(bankAccount, headers);
		
		ResponseEntity<String> response = testRestTemplate.exchange(
				createURLWithPort("/bankAccount/saveBankAccount"),
				HttpMethod.POST, entity, String.class);
		
		
		String response2 = this.testRestTemplate.getForObject("/bankAccount/getall", String.class);
		JSONAssert.assertEquals("[{bankName:melat}]", response2, false);
		
		
		List<BankAccount> list = bankAccountService.findAll();
		assertEquals(1,list.size());
	}
	
	
	
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
