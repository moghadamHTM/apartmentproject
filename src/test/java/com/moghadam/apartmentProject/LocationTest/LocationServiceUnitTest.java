package com.moghadam.apartmentProject.LocationTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;

import com.moghadam.apartmentProject.dao.LocationRepository;
import com.moghadam.apartmentProject.entity.Location;

import com.moghadam.apartmentProject.service.LocationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class LocationServiceUnitTest {
	
	@Mock
	private LocationRepository locationRepository;
	
	@InjectMocks
	private LocationServiceImpl locationService; 
	
	@Test
	public void saveLocation() throws Exception{
		
		Location location=new Location(2,"Southeast","4");
		
		when(locationRepository.save(location)).thenReturn(location);
		
		Assert.assertNotNull(location);
		
		Assert.assertEquals(locationService.save(location),location.getGeographicLocation());
		Assert.assertEquals("Southeast",location.getGeographicLocation());
		Assert.assertEquals("4",location.getFloor());
		
	}
	
	@Test
	public void getAllLocation() {
		Location location1=new Location(1,"Southeast","4");
		Location location2=new Location(2,"northeast","2");
		Location location3=new Location(3,"Southeast","5");
		
		
		List list=new ArrayList<>();
		list.add(location1);
		list.add(location2);
		list.add(location3);
		
		when(locationRepository.findAll()).thenReturn(list);
		
		Assert.assertEquals(locationService.findAll(),list);
		
		Assert.assertEquals(3, list.size());
		
	}
	
	@Test
	public void findLocationById() {
		
		Location location=new Location(2,"northeast","2");
		when(locationRepository.findLocation(2)).thenReturn(location);
		
		
		Location location2 = locationService.findLocationById(2);
		Assert.assertEquals(locationService.findLocationById(2),location2);
		
		Assert.assertEquals("northeast", location2.getGeographicLocation());
		Assert.assertEquals("2", location2.getFloor());	
		
	}
	
	
	
	

}
