package com.moghadam.apartmentProject.LocationTest;

import java.util.List;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.moghadam.apartmentProject.dao.LocationRepository;
import com.moghadam.apartmentProject.entity.Location;
import com.moghadam.apartmentProject.entity.Person;
import com.moghadam.apartmentProject.service.LocationService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class LocationRestControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private LocationService locationService;
	
	HttpHeaders headers = new HttpHeaders();
	
	@Test
	public void saveLocation() throws JSONException {
		Location location=new Location("Southeast","4");
		
		HttpEntity<Location> entity = new HttpEntity<Location>(location, headers);
		
		ResponseEntity<String> response = testRestTemplate.exchange(
				createURLWithPort("/location/saveLocation"),
				HttpMethod.POST, entity, String.class);
		
		String response2 = this.testRestTemplate.getForObject("/location/getall", String.class);
		JSONAssert.assertEquals("[{geographicLocation:Southeast}]", response2, false);
		
		List<Location> list = locationService.findAll();
		assertEquals(1,list.size());
	}
	
	
	
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
