package com.moghadam.apartmentProject.BuildingUnitTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.moghadam.apartmentProject.dao.BuildingUnitRepository;
import com.moghadam.apartmentProject.entity.BuildingUnit;
import com.moghadam.apartmentProject.entity.Location;
import com.moghadam.apartmentProject.service.BuildingUnitServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class BuildingUnitServiceUnitTest {
	
	@Mock
	private BuildingUnitRepository buildingUnitRepository;
	
	@InjectMocks
	private BuildingUnitServiceImpl buildingUnitService;
	
	
	@Test
	public void save() {
		
		BuildingUnit buildingUnit=new BuildingUnit(2,"930000","253","5","40000");
		
		when(buildingUnitRepository.save(buildingUnit)).thenReturn(buildingUnit);
		
		Assert.assertNotNull(buildingUnit);
		
		Assert.assertEquals(buildingUnitService.save(buildingUnit),buildingUnit.getArea());
		Assert.assertEquals("930000",buildingUnit.getFundBalance());
		Assert.assertEquals("40000",buildingUnit.getChargeAmount());
		
	}
	
	@Test
	public void getAllBuildingUnit() {
		
		BuildingUnit buildingUnit1=new BuildingUnit(2,"930000","253","2","40000");
		BuildingUnit buildingUnit2=new BuildingUnit(3,"9650000","190","3","40000");
		BuildingUnit buildingUnit3=new BuildingUnit(5,"8420000","186","5","40000");
		
		List list=new ArrayList<>();
		list.add(buildingUnit1);
		list.add(buildingUnit2);
		list.add(buildingUnit3);
		
		when(buildingUnitRepository.findAll()).thenReturn(list);
		
		Assert.assertEquals(buildingUnitService.findAll(),list);
		
		Assert.assertEquals(3, list.size());
		
	}
	
	
	@Test
	public void findBuildingById() {
		
		BuildingUnit buildingUnit=new BuildingUnit(2,"930000","253","5","40000");
		
		when(buildingUnitRepository.findBuildingUnit(2)).thenReturn(buildingUnit);
		
		
		BuildingUnit buildingUnit2 = buildingUnitService.findBuildingUnitById(2);
		Assert.assertEquals(buildingUnitService.findBuildingUnitById(2),buildingUnit2);
		
		Assert.assertEquals("253", buildingUnit2.getArea());
		Assert.assertEquals("40000", buildingUnit2.getChargeAmount());	
		
	}

}
