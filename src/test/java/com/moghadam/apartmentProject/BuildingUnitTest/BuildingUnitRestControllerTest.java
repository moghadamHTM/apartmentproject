package com.moghadam.apartmentProject.BuildingUnitTest;

import java.util.List;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.moghadam.apartmentProject.dao.BuildingUnitRepository;
import com.moghadam.apartmentProject.entity.BuildingUnit;
import com.moghadam.apartmentProject.entity.Person;
import com.moghadam.apartmentProject.service.BuildingUnitService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class BuildingUnitRestControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@Autowired
	private BuildingUnitRepository buildingUnitRepository;
	
	@Autowired
	private BuildingUnitService buildingUnitService;
	
	
	HttpHeaders headers = new HttpHeaders();
	
	
	@Test
	public void saveBuildingUnit() {
		
		BuildingUnit buildingUnit=new BuildingUnit("930000","253","5","40000");
		
		HttpEntity<BuildingUnit> entity = new HttpEntity<BuildingUnit>(buildingUnit, headers);
		
		ResponseEntity<String> response = testRestTemplate.exchange(
				createURLWithPort("/buildingUnit/saveBuildingUnit"),
				HttpMethod.POST, entity, String.class);
		
		
		List<BuildingUnit> list = buildingUnitService.findAll();
		assertEquals(1,list.size());
		
	}
	
	
	
	
	
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
