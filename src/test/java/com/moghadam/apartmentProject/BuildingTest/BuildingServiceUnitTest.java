package com.moghadam.apartmentProject.BuildingTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.moghadam.apartmentProject.dao.BuildingRepository;
import com.moghadam.apartmentProject.entity.Building;
import com.moghadam.apartmentProject.entity.Location;
import com.moghadam.apartmentProject.service.BuildingServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class BuildingServiceUnitTest {
	
	@Mock
	private BuildingRepository buildingRepository;
	
	@InjectMocks
	private BuildingServiceImpl buildingService;
	
	
	@Test
	public void saveBuilding() {
		
		Building building=new Building(2,"mobin","2586000","35");
		
		when(buildingRepository.save(building)).thenReturn(building);
		
		Assert.assertNotNull(building);
		
		Assert.assertEquals(buildingService.save(building),building.getName());
		Assert.assertEquals("2586000",building.getFundBalance());
		Assert.assertEquals("35",building.getNumberBuildingUnite());
		
	}
	
	@Test
	public void getAllBuilding() {
		Building building1=new Building(2,"mobin","2586000","35");
		Building building2=new Building(3,"yaer","3626000","26");
		Building building3=new Building(4,"sahand","1586000","70");
		
		List list=new ArrayList<>();
		list.add(building1);
		list.add(building2);
		list.add(building3);
		
		when(buildingRepository.findAll()).thenReturn(list);
		
		Assert.assertEquals(buildingService.findAll(),list);
		
		Assert.assertEquals(3, list.size());
	}
	
	@Test
	public void findBuildingById() {
		
		Building building=new Building(4,"sahand","1586000","70");
		when(buildingRepository.findBuilding(4)).thenReturn(building);
		
		
		Building building2 = buildingService.findBuildingById(4);
		Assert.assertEquals(buildingService.findBuildingById(4),building2);
		
		Assert.assertEquals("sahand", building2.getName());
		Assert.assertEquals("70", building2.getNumberBuildingUnite());	
		
	}
	

}
