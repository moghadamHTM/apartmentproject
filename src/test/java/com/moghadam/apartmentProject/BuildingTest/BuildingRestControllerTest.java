package com.moghadam.apartmentProject.BuildingTest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.moghadam.apartmentProject.ApartmentProjectApplication;

import com.moghadam.apartmentProject.entity.Building;

import com.moghadam.apartmentProject.service.BuildingService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApartmentProjectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BuildingRestControllerTest {
	
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@Autowired
	private BuildingService buildingService;

	HttpHeaders headers = new HttpHeaders();
	
	@Test
	public void saveBuilding() throws JSONException {
		
		Building building=new Building("mobin","86000","15");
		
		HttpEntity<Building> entity = new HttpEntity<Building>(building, headers);
		
		ResponseEntity<String> response = testRestTemplate.exchange(
				createURLWithPort("/building/saveBuilding"),
				HttpMethod.POST, entity, String.class);
		
		String response2 = this.testRestTemplate.getForObject("/building/getBuildings", String.class);
		JSONAssert.assertEquals("[{name:mobin}]", response2, false);
		
		List<Building> list = buildingService.findAll();
		assertEquals(1,list.size());
	}
	
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
