package com.moghadam.apartmentProject.AddressTest;


import static org.junit.Assert.assertEquals;

import java.util.List;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.moghadam.apartmentProject.dao.AddressRepository;
import com.moghadam.apartmentProject.entity.Address;
import com.moghadam.apartmentProject.entity.Person;
import com.moghadam.apartmentProject.service.AddressServcie;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class AddressRestControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private AddressServcie addressServcie;
	
	HttpHeaders headers = new HttpHeaders();
	
	@Test
	public void saveAddress() {
		

		Address address=new Address("tehran","pardis","ponak","465616546","784512");
		
		HttpEntity<Address> entity = new HttpEntity<Address>(address, headers);
		
		ResponseEntity<String> response = testRestTemplate.exchange(
				createURLWithPort("/address/saveAddress"),
				HttpMethod.POST, entity, String.class);
		
		
		List<Address> list = addressServcie.findAll();
		assertEquals(1,list.size());
		
	}
	
	@Test
	public void getAllAddress() throws JSONException {
		
		String response = this.testRestTemplate.getForObject("/address/getall", String.class);
		JSONAssert.assertEquals("[{city:pardis}]", response, false);
		
		List<Address> list = (List<Address>) addressRepository.findAll();
		assertEquals(1,list.size());
		
	}
	
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
