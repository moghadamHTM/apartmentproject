package com.moghadam.apartmentProject.AddressTest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.moghadam.apartmentProject.dao.AddressRepository;
import com.moghadam.apartmentProject.entity.Address;
import com.moghadam.apartmentProject.entity.Location;
import com.moghadam.apartmentProject.service.AddressServcieImpl;

@RunWith(MockitoJUnitRunner.class)
public class AddressServiceUnitTest {
	
	@Mock
	private AddressRepository addressRepository;
	
	@InjectMocks
	private AddressServcieImpl addressServcie;
	
	
	
	@Test
	public void saveAddress() throws Exception{
		
		Address address=new Address(2,"tehran","pardis","ponak","465616546","784512");
		
		when(addressRepository.save(address)).thenReturn(address);
		
		Assert.assertNotNull(address);
		
		Assert.assertEquals(addressServcie.save(address),address.getCity());
		Assert.assertEquals("pardis",address.getCity());
		Assert.assertEquals("465616546",address.getPostalAddress());
		
	}
	
	
	@Test
	public void getAllAddress() {
		Address address1=new Address(2,"tehran","pardis","ponak","465616546","784512");
		Address address2=new Address(3,"gilan","rasht","rajae","8521456","96345");
		Address address3=new Address(4,"shiraz","razan","moradi","1236587","954876");
		Address address4=new Address(5,"karaj","shahrayar","mohamadi","8514545","15654654");
		
		List list=new ArrayList<>();
		list.add(address1);
		list.add(address2);
		list.add(address3);
		list.add(address4);
		
		when(addressRepository.findAll()).thenReturn(list);
		
		Assert.assertEquals(addressServcie.findAll(),list);
		
		Assert.assertEquals(4, list.size());
		
	}
	
	@Test
	public void findAddressById() {
		
		Address address1=new Address(2,"tehran","pardis","ponak","465616546","784512");
		when(addressRepository.findAddress(2)).thenReturn(address1);
		
		
		Address address2 = addressServcie.findAddressById(2);
		Assert.assertEquals(addressServcie.findAddressById(2),address2);
		
		Assert.assertEquals("pardis", address2.getCity());
		Assert.assertEquals("465616546", address2.getPostalAddress());	
		
	}

}
