package com.moghadam.apartmentProject.PersonTest;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.moghadam.apartmentProject.controller.PersonRestController;
import com.moghadam.apartmentProject.dao.PersonRepository;
import com.moghadam.apartmentProject.entity.Person;
import com.moghadam.apartmentProject.service.PersonService;
import com.moghadam.apartmentProject.service.PersonServiceImpl;

import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.mock;


@RunWith(MockitoJUnitRunner.class)
public class PersonServiceUnitTest {
	
	@Mock
	private PersonRepository personRepository;
	
	@InjectMocks
	private PersonServiceImpl personService;
	
	
	
	@Test
	public void savePerson() throws Exception{
		
		/*//when(personRepository.save()).thenReturn(new Person(2,"ali","moradi","moaven","malek","09198681049","02536625221"));
		
		Person person=new Person(2,"ali","moradi","moaven","09198681049","02536625221");
		
		when(personService.save(person)).thenReturn(person);
		
		RequestBuilder requst=MockMvcRequestBuilders.get("/person/savePerson").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requst).andExpect(status().isOk()).andExpect(content().json("{id:2,name:ali,family:moradi,personType:moaven,mobilePhone:09198681049,phone:02536625221}")).andReturn();	
		
		//assertEquals("i am hasan moghadam!!",result.getResponse().getContentAsString());
*/	
		
		/*when(personRepository.save(Person.class)).thenReturn(new Person());

		Person person=new Person();

        assertThat(service.addCustomer(customer), is(notNullValue()));*/
		
		
		
		Person person=new Person(2,"ali","moradi","moaven","09198681049","02536625221");
		
		when(personRepository.save(person)).thenReturn(person);
		
		Assert.assertNotNull(person);
		
		Assert.assertEquals(personService.save(person),person.getFamily());
		Assert.assertEquals("moradi",person.getFamily());
		Assert.assertEquals("ali",person.getName());
		
		/*String family = personService.save(person);
		
		assertNotNull(family);
		assertEquals("moradi", family);*/
		
	}
	
	@Test
	public void getAllPerson() {
		
		Person person1=new Person(1,"ali","moradi","moaven","09198681049","02536625221");
		Person person2=new Person(2,"hasan","moghadam","modir","09128688549","02136625221");
		Person person3=new Person(3,"mahdi","shrifi","modir","09038688547","01316625221");
		List list=new ArrayList<>();
		list.add(person1);
		list.add(person2);
		list.add(person3);
		
		when(personRepository.findAll()).thenReturn(list);
		
		Assert.assertEquals(personService.findAll(),list);
		
		Assert.assertEquals(3, list.size());
		
	}
	
	@Test
	public void findPersonById() {
		
		Person person=new Person(3,"mahdi","shrifi","modir","09038688547","01316625221");
		when(personRepository.findPersonById(3)).thenReturn(person);
		
		
		Person person2 = personService.findPersonById(3);
		Assert.assertEquals(personService.findPersonById(3),person2);
		
		Assert.assertEquals("mahdi", person2.getName());
		Assert.assertEquals("shrifi", person2.getFamily());	
		
	}
	
	
	@Test
	public void findByFamily() {
		
		String family="moghadam";
		
		when(personRepository.findByFamily(family)).thenReturn(new Person(2,"hasan","moghadam","modir","09128688549","02136625221"));
		
		Person person4 = personService.findByFamily(family);
		
		
		Assert.assertEquals("hasan", person4.getName());
		
	}
	
	
	

}
