package com.moghadam.apartmentProject.PersonTest;

import java.util.List;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.moghadam.apartmentProject.dao.PersonRepository;

import com.moghadam.apartmentProject.entity.Person;
import com.moghadam.apartmentProject.service.PersonService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class PersonRestControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private PersonService personService;
	
	HttpHeaders headers = new HttpHeaders();
	
	@Test
	public void saveBuilding() throws JSONException {
		
		Person person=new Person("hasan","moghadam","malek","09198681049","02536625221");
		
		HttpEntity<Person> entity = new HttpEntity<Person>(person, headers);
		
		ResponseEntity<String> response = testRestTemplate.exchange(
				createURLWithPort("/person/savePerson"),
				HttpMethod.POST, entity, String.class);
		
		
		List<Person> list = personService.findAll();
		assertEquals(1,list.size());
	}
	
	
	@Test
	public void getAllPerson() throws Exception {
		String response = this.testRestTemplate.getForObject("/person/getall", String.class);
		JSONAssert.assertEquals("[{family:moghadam}]", response, false);
		
		List<Person> list = (List<Person>) personRepository.findAll();
		assertEquals(1,list.size());
	}
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
	
	

}
